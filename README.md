A small Python 2 script that uses Adam Caudill's website
https://hstspreload.com

to check if a domain/website is in the Strict Transport Security list in Firefox, Chrome, and Tor. 


It appends only the first command line argument to 
https://hstspreload.com/api/v1/status/

and uses urllib2.urlopen to visit the url and save the response. The response is json formatted so it calls json.loads to save it as a Python object. This way, it's possible to print out the response using json.dumps with an indent of 2 so it is easily readable.