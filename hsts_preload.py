import sys,json, urllib2

if not len(sys.argv) == 2:
  print "Usage: " + sys.argv[0] + " domain.name"
  print "Website: https://hstspreload.com/"
  print
  exit()

req = urllib2.Request("https://hstspreload.com/api/v1/status/" + sys.argv[1])
response = urllib2.urlopen(req)
result = response.read()

data = json.loads(result)
print json.dumps(data, indent=2)